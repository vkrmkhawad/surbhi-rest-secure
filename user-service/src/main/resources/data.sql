INSERT INTO items (id, name, price)
SELECT * FROM (SELECT '1' AS id, 'Pasta' AS name, '35' AS price) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM items WHERE id = '1'
) LIMIT 1;

INSERT INTO items (id, name, price)
SELECT * FROM (SELECT '2' AS id, 'Pizza' AS name, '85' AS price) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM items WHERE id = '2'
) LIMIT 1;

INSERT INTO items (id, name, price)
SELECT * FROM (SELECT '3' AS id, 'Noodles' AS name, '25' AS price) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM items WHERE id = '3'
) LIMIT 1;

INSERT INTO items (id, name, price)
SELECT * FROM (SELECT '4' AS id, 'Lasagne' AS name, '78' AS price) AS tmp
WHERE NOT EXISTS (
    SELECT name FROM items WHERE id = '4'
) LIMIT 1;

INSERT INTO users (id, email, first_name, last_name, password, role, user_name)
SELECT * FROM (SELECT '1' AS id, 'admin@admin.com' AS email, 'admin' AS first_name, 'admin' AS last_name, '$2a$10$wtxBj0Cp1r20yzAyMo5aruyptVDPBXq79oL4goSI3z4X7u0nehZja' AS password, 'ADMIN' AS role, 'admin' AS user_name) AS tmp
WHERE NOT EXISTS (
    SELECT email FROM users WHERE id = '1'
) LIMIT 1;
