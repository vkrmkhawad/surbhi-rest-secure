use surabi_restaurant;
show procedure status where db='surabi_restaurant';


// fetch all orders view
create view orders as select * from financial_report;

// fetch by price procedure
DELIMITER //
CREATE PROCEDURE FetchOrderByPrice(IN priceInput int)
BEGIN
SELECT * FROM financial_report where price = priceInput;
END//
DELIMITER;

// fetch by purchase_date procedure
DELIMITER //
CREATE PROCEDURE FetchOrderByDate(IN purchaseDateInput varchar(200))
BEGIN
select * from financial_report where DATE_FORMAT(purchase_date, '%d/%m/%Y') =  purchaseDateInput;
END//
DELIMITER;

// fetch all orders group by cities
create view SalesGroupByCities as select city, sum(price) as sale from financial_report group by city;
// fetch max sale
create view MaxSale as select max(price) as max_sale from financial_report;

// fetch all orders group by cities
create view SalesGroupByMonths as select extract(month from purchase_date), sum(price) as sale from financial_report group by extract(month from purchase_date);

