package com.service.festivesales.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/festiveOffers")
public class FestiveOfferController {

	@PostMapping("/applyOffer")
	public String applyOffer(String couponCode) {
		return "Coupon applied to get 50% discount";
	}
}
