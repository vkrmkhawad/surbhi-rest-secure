package com.service.surbhiservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.surbhiservice.feignclients.AdminClient;
import com.service.surbhiservice.model.FinancialReportDetails;
import com.service.surbhiservice.model.UserDetails;

@RestController("/surabi/admin")
public class AdminController {

	@Autowired
	AdminClient adminClient;

	@GetMapping("/users/addFromSurabi")
	public String addUser(UserDetails user) {
		return adminClient.addUser(user);
	}

	@GetMapping("/users/updateFromSurabi")
	public String updateUser(UserDetails user) {
		return adminClient.updateUser(user);
	}

	@GetMapping("/users/fetchFromSurabi")
	public UserDetails fetchUser(int id) {
		return adminClient.fetchUser(id);
	}

	@GetMapping("/users/deleteFromSurabi")
	public String deleteUser(int id) {
		return adminClient.deleteUser(id);
	}

	@GetMapping("/viewDailyBillingsFromSurabi")
	public List<FinancialReportDetails> viewDailyBilling() {
		return adminClient.getDailyReport();
	}

	@GetMapping("/viewMonthlyBillingsFromSurabi")
	public List<FinancialReportDetails> viewMonthlyBilling() {
		return adminClient.getMonthlyReport();
	}
}
